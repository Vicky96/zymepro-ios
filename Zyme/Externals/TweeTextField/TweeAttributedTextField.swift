//  Zyme
//
//  Created by Vicky Singh on 16/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit
protocol MyTextFieldDelegate {
    func textFieldDidDelete(obj : TweeAttributedTextField)
}

/// An object of the class can show the custom info label under text field.
open class TweeAttributedTextField: TweeActiveTextField {
    var myDelegate: MyTextFieldDelegate?
	/// Info label that is shown for a user. This label will appear under the text field.
	/// You can use it to configure appearance.
	public private(set) lazy var infoLabel = UILabel()

	/// Animation duration for showing and hiding the info label.
	@IBInspectable public private(set) var infoAnimationDuration: Double = 1

	/// Color of info text.
	@IBInspectable public var infoTextColor: UIColor {
		get {
			return infoLabel.textColor
		} set {
			infoLabel.textColor = UIColor.red
		}
	}

	/// Font size of info text. If you want to change font use `infoLabel` property.
	@IBInspectable public var infoFontSize: CGFloat {
		get {
			return infoLabel.font.pointSize
		} set {
			infoLabel.font = infoLabel.font.withSize(newValue)
		}
	}

	// MARK: Methods
    override open func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete(obj: self)
    }
    
	public override init(frame: CGRect) {
		super.init(frame: frame)
		initializeSetup()
	}

	public required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initializeSetup()
	}

	private func initializeSetup() {
//        if "\(UserDefaults.standard.value(forKey: CurrentLanguageKey) ?? "")" == "ar" || "\(UserDefaults.standard.value(forKey: CurrentLanguageKey) ?? "")" == "he" || "\(UserDefaults.standard.value(forKey: CurrentLanguageKey) ?? "")" == "ur" {
        
            if self.textAlignment == .right {
                self.textAlignment = .left
            } else if self.textAlignment == .left {
                self.textAlignment = .right
            } else {
                 self.textAlignment = .left
            }
        
		plugInfoLabel()
    }

	/// Shows info label with/without animation.
	/// - Parameters:
	///   - text: Custom attributed text to show.
	///   - animated: By default is `true`.
	public func showInfo(_ attrText: NSAttributedString, animated: Bool = true) {
		if animated {
			UIView.transition(with: infoLabel, duration: infoAnimationDuration, options: [.transitionCrossDissolve], animations: {
				self.infoLabel.alpha = 1
				self.infoLabel.attributedText = attrText
			})
		} else {
			infoLabel.attributedText = attrText
		}
	}

	/// Shows info label with/without animation.
	/// - Parameters:
	///   - text: Custom text to show.
	///   - animated: By default is `true`.
	public func showInfo(_ text: String, animated: Bool = true) {
		if animated {
			UIView.transition(with: infoLabel, duration: infoAnimationDuration, options: [.transitionCrossDissolve], animations: {
				self.infoLabel.alpha = 1
				self.infoLabel.text = text
			})
		} else {
			infoLabel.text = text
		}
	}

	/// Hides the info label with animation or not.
	/// - Parameter animated: By default is `true`.
	public func hideInfo(animated: Bool = true) {
		if animated {
			UIView.animate(withDuration: infoAnimationDuration) {
				self.infoLabel.alpha = 0
			}
		} else {
			infoLabel.alpha = 0
		}
	}

	private func plugInfoLabel() {
		if infoLabel.superview != nil {
			return
		}
        infoLabel.numberOfLines = 2
        
		addSubview(infoLabel)
        
		infoLabel.translatesAutoresizingMaskIntoConstraints = false

		infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
		infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
		infoLabel.topAnchor.constraint(equalTo: topAnchor, constant: 40).isActive = true
//        infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 2).isActive = true
	}
}

//  Zyme
//
//  Created by Vicky Singh on 16/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import Foundation
import QuartzCore
import CoreFoundation

internal final class FontAnimation {

	private let displayLink: CADisplayLink?
	private(set) var startTime: CFTimeInterval?

	private let target: Any
	private let selector: Selector

	init(target: Any, selector: Selector) {
		self.target = target
		self.selector = selector

		displayLink = CADisplayLink(target: target, selector: selector)
		displayLink?.add(to: .main, forMode: .common)

		if #available(iOS 10.0, *) {
			displayLink?.preferredFramesPerSecond = 30
		}
	}

	func start() {
		startTime = CFAbsoluteTimeGetCurrent()
		displayLink?.isPaused = false
	}

	func stop() {
		startTime = nil
		displayLink?.isPaused = true
	}
}

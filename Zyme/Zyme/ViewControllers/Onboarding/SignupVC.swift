//
//  SignupVC.swift
//  Zyme
//
//  Created by Vicky Singh on 17/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

        @IBOutlet weak var signUpTableView: UITableView!
    
        
        //Private Properties
        //Public Properties
        //Private Computed Properties
        let placeholderArray = ["Name","Email","Mobile","Password"]
        var userModel = UserDetailsModel()
    }

    // MARK: - View Life Cycle
    extension SignupVC {
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
        }
        override func viewWillAppear(_ animated: Bool) {
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        
    }
    // MARK: - Target Actions
    extension SignupVC {
        
        @objc func showHidePassword(_ sender: UIButton) {
            let index = sender.tag-2500
            let indexPath = IndexPath(row: index, section: 0)
            let cell = signUpTableView.cellForRow(at: indexPath) as! TextFieldCell
            cell.inputTextField.isSecureTextEntry.toggle()
            if cell.inputTextField.isSecureTextEntry {
                cell.visiblePasswordButton.setImage(UIImage(named: "icHide"), for: .normal)
            } else {
                cell.visiblePasswordButton.setImage(UIImage(named: "icVisibility"), for: .normal)
            }
        }
        
    }

    // MARK: - IBActions
    extension SignupVC {
        @IBAction func signupAction(_ sender: UIButton ){
            self.view.endEditing(true)
            let FirstStepVC = ViewControllerAccessors.addDeviceFirstStepVC
            self.navigationController?.pushViewController(FirstStepVC, animated: true)
            if validateFields() {
              // Call Api to signup
                print("+++++++ Signup Suceessfull ++++++")
            }
        }
        
        @IBAction func agreeButtonAction(_ sender: UIButton) {
               sender.isSelected = !sender.isSelected
               userModel.isAgreeSelected = !userModel.isAgreeSelected
           }
           @IBAction func termsBtnAction(_ sender: UIButton) {
//               let termsVC = ViewControllerAccessor.termsViewController
//               self.navigationController?.pushToViewControllerWithFadeAnimation(termsVC)
            print("Terms button Tapped")
           }
       
    }
    // MARK: - Private Functions
    extension SignupVC {
        //MARK:- Function for validation
        fileprivate func validateFields() -> Bool {
            var isVerified = false
            if userModel.name.isEmpty {
                userModel.errorIndex = 0
                userModel.errorMessage = enterName
            } else if !userModel.name.isValidName() {
                userModel.errorIndex = 0
                userModel.errorMessage = validName
            } else if (userModel.email.isEmpty){
                userModel.errorIndex = 1
                userModel.errorMessage = enterEmail
            } else if (userModel.email.length < 8){
                userModel.errorIndex = 1
                userModel.errorMessage = validEmail
            } else if (!userModel.email.isEmail()){
                userModel.errorIndex = 1
                userModel.errorMessage = validEmail
            } else if userModel.mobile.isEmpty {
                userModel.errorIndex = 2
                userModel.errorMessage = enterNumber
            } else if !userModel.mobile.isValidMobileNumber() {
                userModel.errorIndex = 2
                userModel.errorMessage = validNumber
            } else if (userModel.password.isEmpty){
                userModel.errorIndex = 3
                userModel.errorMessage = enterPassword
            } else if (userModel.password.length < 6){
                userModel.errorIndex = 3
                userModel.errorMessage = passwordMinLength
            } else {
                userModel.errorIndex = -1
                userModel.errorMessage = ""
                 if !userModel.isAgreeSelected {
                    AlertController.alert(title: "Error!!", message: "*Please select Terms & Conditions.")
                } else {
                    isVerified = true
                }
            }
            signUpTableView.reloadData()
            return isVerified
        }
    }

    // MARK: - Delegates
    extension SignupVC: UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
        
        //UITableViewDelegate,UITableViewDataSource methods
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return placeholderArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let signUp = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
            signUp.inputTextField.tweePlaceholder = placeholderArray[indexPath.row]
            signUp.inputTextField.tag = indexPath.row + 200
            signUp.inputTextField.isSecureTextEntry = false
            signUp.visiblePasswordButton.isHidden = true
            
            signUp.visiblePasswordButton.tag = indexPath.row+2500
            if placeholderArray[indexPath.row] == "Password"{
                signUp.visiblePasswordButton.isHidden = false
                signUp.inputTextField.isSecureTextEntry = true
                signUp.visiblePasswordButton.setImage(UIImage(named: "icHide"), for: .normal)
                signUp.visiblePasswordButton.isSelected = false
                signUp.visiblePasswordButton.addTarget(self, action: #selector(showHidePassword(_ :)), for: .touchUpInside)
            } else {
                signUp.visiblePasswordButton.isHidden = true
                signUp.inputTextField.isSecureTextEntry = false
            }
            
            if userModel.errorIndex == indexPath.row{
                signUp.errorLabel.text = userModel.errorMessage
            } else{
                signUp.errorLabel.text = ""
            }
            
            switch indexPath.row {
            case 0:
                signUp.inputTextField.text = userModel.name
                signUp.inputTextField.autocapitalizationType = .words
                signUp.inputTextField.returnKeyType = .next
                signUp.inputImageView.image = #imageLiteral(resourceName: "avatar")
                break
            case 1:
                signUp.inputTextField.text = userModel.email
                signUp.inputTextField.keyboardType = .emailAddress
                signUp.inputTextField.returnKeyType = .next
                signUp.inputImageView.image = #imageLiteral(resourceName: "envelope")
                break
            case 2:
                signUp.inputTextField.text = userModel.mobile
                signUp.inputTextField.keyboardType = .numberPad
                signUp.inputTextField.returnKeyType = .next
                signUp.inputImageView.image = #imageLiteral(resourceName: "phone-call")
                break
            case 3:
                signUp.visiblePasswordButton.isHidden = false
                signUp.inputTextField.isSecureTextEntry = true
                signUp.inputTextField.text = userModel.password
                signUp.inputTextField.keyboardType = .asciiCapable
                signUp.inputTextField.returnKeyType = .done
                signUp.inputImageView.image = #imageLiteral(resourceName: "key")
                
                break
            default:
                break
            }
            return signUp
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
        
        //UITextFieldDelegate methods
        func textFieldDidEndEditing(_ textField: UITextField) {
            switch textField.tag {
            case 200:
                userModel.name = textField.text!
                break
            case 201:
                let str = textField.text!
                let trimmedString = str.trimmingCharacters(in: .whitespaces)
                userModel.email = trimmedString
                break
            case 202:
                userModel.mobile = textField.text!
                break
            case 203:
                userModel.password = textField.text!
                break
            default:
                break
            }
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let textLength = textField.text! + string
            
            switch textField.tag {
            case 200:
                if textLength.count > 64 {  //Name
                    return false
                } else {
                    return true
                }
            case 201:
                if textLength.count > 256 { //Email
                    return false
                } else {
                    return true
                }
            case 202:
                if textLength.count > 10 { //Mobile
                    return false
                } else {
                    return true
                }
            case 203:
                if textLength.count > 16 { //Password
                    return false
                } else {
                    return true
                }
            default:
                break
            }
            return true
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField.returnKeyType == .next {
                let field = self.view.viewWithTag(textField.tag+1)
                field?.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
            return true
        }
        
        
    }




//
//  AddDeviceFourthStepVC.swift
//  Zyme
//
//  Created by Vicky Singh on 17/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class AddDeviceFourthStepVC: UIViewController {

   // MARK: - IBOutlet
       @IBOutlet weak var FourthStepTableView: UITableView!
       
       var userModel = UserDetailsModel()
        
    

           }

           // MARK: - View Life Cycle
           extension AddDeviceFourthStepVC {
               
               override func viewDidLoad() {
                   super.viewDidLoad()
                   // Do any additional setup after loading the view, typically from a nib.
                let buttonCell = UINib.init(nibName: "CustomButtonCell", bundle: nil)
                               self.FourthStepTableView.register(buttonCell, forCellReuseIdentifier: "CustomButtonCell")
                userModel.petrolIsSelected = true
                userModel.dieselIsSelected = false
                
                              
                FourthStepTableView.reloadData()
                   
               }
               
               override func viewWillAppear(_ animated: Bool) {
                   
               }
               
               override func didReceiveMemoryWarning() {
                   super.didReceiveMemoryWarning()
                   // Dispose of any resources that can be recreated.
               }
               
               override var preferredStatusBarStyle: UIStatusBarStyle {
                   return .lightContent
               }
               
           }

// MARK: - Target Actions
       extension AddDeviceFourthStepVC {
           
        @objc func buttonFirstTapped(_ sender: UIButton) {
            self.view.endEditing(true)
            print("First Button tapped")
            userModel.petrolIsSelected = false
            userModel.dieselIsSelected = true
            // Mark:-  Brand button Selected
        
//            let dummyList = ["Audi", "BMW", "Chevrolet", "Datsun", "Dodge", "Fiat"]
//
//
//                        RPicker.selectOption(dataArray: dummyList) {[weak self] (selctedText, atIndex) in
//                            // TODO: Your implementation for selection
//            //                self?.outputLabel.text = selctedText + " selcted at \(atIndex)"
//                            print("Selected Model :-",selctedText)
//
//                        }
            FourthStepTableView.reloadData()
            
        }
        
        @objc func buttonSecondTapped(_ sender: UIButton) {
            self.view.endEditing(true)
           print("Second Button tapped")
            let dummyList = ["Audi", "BMW", "Chevrolet", "Datsun", "Dodge", "Fiat"]

            // Mark:-  Model button Selected
            RPicker.selectOption(dataArray: dummyList) {[weak self] (selctedText, atIndex) in
                // TODO: Your implementation for selection
//                self?.outputLabel.text = selctedText + " selcted at \(atIndex)"
                print("Selected Model :-",selctedText,"atIndex :-",atIndex)
                
            }
            
            // Mark:-  State button Selected
//             let dummyList = ["Audi", "BMW", "Chevrolet", "Datsun", "Dodge", "Fiat"]
//
//                        // Mark:-  Model button Selected
//                        RPicker.selectOption(dataArray: dummyList) {[weak self] (selctedText, atIndex) in
//                            // TODO: Your implementation for selection
//            //                self?.outputLabel.text = selctedText + " selcted at \(atIndex)"
//                            print("Selected Model :-",selctedText)
//
//                        }
            
            FourthStepTableView.reloadData()
            
        }
        
        @objc func buttonThirdTapped(_ sender: UIButton) {
            self.view.endEditing(true)
            print("Third Button tapped")
         userModel.petrolIsSelected = true
         userModel.dieselIsSelected = false
            // Mark:-  CC button Selected
//            let dummyList = ["Audi", "BMW", "Chevrolet", "Datsun", "Dodge", "Fiat"]
//
//
//                        RPicker.selectOption(dataArray: dummyList) {[weak self] (selctedText, atIndex) in
//                            // TODO: Your implementation for selection
//            //                self?.outputLabel.text = selctedText + " selcted at \(atIndex)"
//                            print("Selected Model :-",selctedText)
//
//                        }
         FourthStepTableView.reloadData()
            
        }
           
       }

     // MARK: - IBActions
        extension AddDeviceFourthStepVC {
            @IBAction func continueAction(_ sender: UIButton ){
                self.view.endEditing(true)
                if validateFields() {
                  print("++++++++Continue Tapped Success++++++")
                }
            }
            
          

        }
           // MARK: - Private Functions
           extension AddDeviceFourthStepVC {
               // Function for validation
               fileprivate func validateFields() -> Bool {
                   var isVerified = false
                   let str = userModel.petName
                   let trimmedString = str.trimmingCharacters(in: .whitespaces)
                   userModel.petName = trimmedString
                   if (userModel.petName.isEmpty){
                       userModel.errorIndex = 0
                       userModel.errorMessage = "*Please enter pet name"
                   } else if (userModel.vehicleNumber.isEmpty){
                       userModel.errorIndex = 1
                       userModel.errorMessage = "*Please enter vehicle number"
                   } else if (userModel.vehicleNumber.length < 8){
                       userModel.errorIndex = 1
                       userModel.errorMessage = "*Please enter valid vehicle number"
                   } else {
                       userModel.errorIndex = -1
                       userModel.errorMessage = ""
                       isVerified = true
                   }
                   FourthStepTableView.reloadData()
                   return isVerified
               }
           }

           // MARK: - Delegates
           extension AddDeviceFourthStepVC: UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
               
               //UITableViewDelegate,UITableViewDataSource methods
               func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                   return 5
               }
               
               func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                   let textFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
                   textFieldCell.inputTextField.tag = indexPath.row + 300
                    textFieldCell.visiblePasswordButton.isHidden = true
                
           //        loginCell.inputTextField.isSecureTextEntry = false
               
                   if userModel.errorIndex == indexPath.row {
                       textFieldCell.errorLabel.text = userModel.errorMessage
                   } else{
                       textFieldCell.errorLabel.text = ""
                   }
                        switch indexPath.row {
                        case 0,1:
                            if indexPath.row == 0 {
                                               textFieldCell.inputTextField.tweePlaceholder = " My Batmobile"
                                                  textFieldCell.inputTextField.text = userModel.petName
                                                  textFieldCell.inputTextField.keyboardType = .asciiCapable
                                                  textFieldCell.inputTextField.returnKeyType = .next
                                                  textFieldCell.inputImageView.image = #imageLiteral(resourceName: "avatar")
                                              } else if indexPath.row == 1 {
                                               textFieldCell.inputTextField.tweePlaceholder = " DL XX AC XXXX"
                                                  textFieldCell.inputTextField.text = userModel.vehicleNumber
                                                  textFieldCell.inputTextField.keyboardType = .asciiCapable
                                                  textFieldCell.inputTextField.returnKeyType = .done
                                                  textFieldCell.inputTextField.isSecureTextEntry = true
                                                  textFieldCell.inputImageView.image = #imageLiteral(resourceName: "sports-car")
                                           }
                            
                            return textFieldCell
                            case 2,3,4:
                                let customButtonCell = tableView.dequeueReusableCell(withIdentifier: "CustomButtonCell") as! CustomButtonCell
                                customButtonCell.buttonFirst.addTarget(self, action: #selector(buttonFirstTapped(_ :)), for: .touchUpInside)
                                customButtonCell.buttonSecond.addTarget(self, action: #selector(buttonSecondTapped(_ :)), for: .touchUpInside)
                                customButtonCell.buttonThird.addTarget(self, action: #selector(buttonThirdTapped(_ :)), for: .touchUpInside)
                                       customButtonCell.seperatorViewFirst.isHidden = true
                                       customButtonCell.seperatorViewSecond.isHidden = true
                                       if indexPath.row == 2 {
                                               customButtonCell.buttonFirst.setTitle("Brand",for: .normal)
                                               customButtonCell.buttonSecond.setTitle("Model",for: .normal)
                                               customButtonCell.buttonThird.setTitle("CC",for: .normal)
                                           customButtonCell.seperatorViewFirst.isHidden = false
                                           customButtonCell.seperatorViewSecond.isHidden = false
                                           customButtonCell.buttonThird.isHidden = false
                                           customButtonCell.buttonSecond.isHidden = false
                                           customButtonCell.buttonFirst.isHidden = false
                                               
                                       } else if indexPath.row == 3 {
                                               customButtonCell.buttonSecond.setTitle("Select state",for: .normal)
                                               customButtonCell.buttonThird.isHidden = true
                                               customButtonCell.buttonFirst.isHidden = true
                                               
                                       } else if indexPath.row == 4 {
                                           if userModel.petrolIsSelected == true {
                                              customButtonCell.buttonFirst.setImage(#imageLiteral(resourceName: "radio-on-button (1)"), for: .normal)
                                               customButtonCell.buttonThird.setImage(#imageLiteral(resourceName: "radio-on-button"), for: .normal)
                                                customButtonCell.buttonThird.tintColor = #colorLiteral(red: 0.1795614958, green: 0.6856242418, blue: 0.9090497494, alpha: 1)
                                                customButtonCell.buttonFirst.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                                            
                                           } else if userModel.dieselIsSelected == true {
                                               customButtonCell.buttonThird.setImage(#imageLiteral(resourceName: "radio-on-button (1)"), for: .normal)
                                               customButtonCell.buttonFirst.setImage(#imageLiteral(resourceName: "radio-on-button"), for: .normal)
                                            customButtonCell.buttonThird.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                                            customButtonCell.buttonFirst.tintColor = #colorLiteral(red: 0.1795614958, green: 0.6856242418, blue: 0.9090497494, alpha: 1)
                                                
                                           }
                                           customButtonCell.topView.backgroundColor = .black
                                           customButtonCell.topViewLeadingConstraint.constant = 60
                                           customButtonCell.topViewTrailingConstraint.constant = 60
                                           
                                           customButtonCell.buttonFirst.setTitle("Diesel",for: .normal)
                                           customButtonCell.buttonThird.setTitle("Petrol",for: .normal)
                                           customButtonCell.buttonSecond.isHidden = true
                                           
                                          }
                                       return customButtonCell
                                    default:
                                        return UITableViewCell()
                                    }
                   
               }
               
               func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                   return UITableView.automaticDimension
               }
               
               //UITextFieldDelegate methods
               func textFieldDidEndEditing(_ textField: UITextField) {
                   switch textField.tag {
                   case 300:
                       userModel.petName = textField.text!
                       break
                   case 301:
                       userModel.vehicleNumber = textField.text!
                       break
                   default:
                       break
                   }
               }
               
               func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                   let textLength = textField.text! + string
                   
                   
                   switch textField.tag {
                   case 300:
                       if textLength.count > 150 {
                           return false
                       } else {
                           return true
                       }
                   case 301:
                       if textLength.count > 16 {
                           return false
                       } else {
                           return true
                       }
                   default:
                       break
                   }
                   return true
               }
               
               func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                   if textField.returnKeyType == .next {
                       let field = self.view.viewWithTag(textField.tag+1)
                       field?.becomeFirstResponder()
                   } else {
                       textField.resignFirstResponder()
                   }
                   return true
               }
               
           }






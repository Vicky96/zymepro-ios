//
//  LoginVC.swift
//  Zyme
//
//  Created by Vicky Singh on 16/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

        @IBOutlet weak var loginTableView: UITableView!
        //Private Properties
        //Public Properties
        //Private Computed Properties
        let placeholderArray = [
            "Email",
            "Password"
        ]
       
        var userModel = UserDetailsModel()
    }

    // MARK: - View Life Cycle
    extension LoginVC {
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        
    }
    // MARK: - Target Actions
    extension LoginVC {
        @objc func showHidePassword(_ sender: UIButton) {
            let index = sender.tag-2000
            let indexPath = IndexPath(row: index, section: 0)
            let cell = loginTableView.cellForRow(at: indexPath) as! TextFieldCell
            cell.inputTextField.isSecureTextEntry.toggle()
            if cell.inputTextField.isSecureTextEntry {
                cell.visiblePasswordButton.setImage(UIImage(named: "icHide"), for: .normal)
            } else {
                cell.visiblePasswordButton.setImage(UIImage(named: "icVisibility"), for: .normal)
            }
        }
        
    }

    // MARK: - IBActions
    extension LoginVC {
        @IBAction func loginAction(_ sender: UIButton ){
            self.view.endEditing(true)
            if validateFields() {
              print("++++++++Login Success++++++")
            }
        }
        
        @IBAction func forgotPasswordAction(_ sender: UIButton) {
            
//                    let registeredEmailVC = ViewControllerAccessors.registeredEmailViewController
//                    self.navigationController?.pushViewController(registeredEmailVC, animated: true)
        }

    }

    // MARK: - Target Actions
    extension LoginVC {
    //    @IBAction func backAction(_ sender: UIButton) {
    //        self.navigationController?.popViewController(animated: true)
    //    }
    }

    // MARK: - Private Functions
    extension LoginVC {
        //MARK:- Function for validation
        fileprivate func validateFields() -> Bool {
            var isVerified = false
            let str = userModel.email
            let trimmedString = str.trimmingCharacters(in: .whitespaces)
            userModel.email = trimmedString
            if (userModel.email.isEmpty){
                userModel.errorIndex = 0
                userModel.errorMessage = enterEmail
            } else if (userModel.email.length < 8){
                userModel.errorIndex = 0
                userModel.errorMessage = validEmail
            } else if (!userModel.email.isEmail()){
                userModel.errorIndex = 0
                userModel.errorMessage = validEmail
            } else if (userModel.password.isEmpty){
                userModel.errorIndex = 1
                userModel.errorMessage = enterPassword
            } else if (userModel.password.length < 6){
                userModel.errorIndex = 1
                userModel.errorMessage = passwordMinLength
            } else {
                userModel.errorIndex = -1
                userModel.errorMessage = ""
                isVerified = true
            }
            loginTableView.reloadData()
            return isVerified
        }
    }

    // MARK: - Delegates
    extension LoginVC: UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
        
        //UITableViewDelegate,UITableViewDataSource methods
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return placeholderArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let loginCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
            loginCell.inputTextField.tweePlaceholder = placeholderArray[indexPath.row]
            loginCell.inputTextField.tag = indexPath.row + 100
            
            
    //        loginCell.inputTextField.isSecureTextEntry = false
            
            loginCell.visiblePasswordButton.tag = indexPath.row+2000
            if placeholderArray[indexPath.row] == "Password" {
                loginCell.visiblePasswordButton.isHidden = false
                loginCell.inputTextField.isSecureTextEntry = true
                loginCell.visiblePasswordButton.setImage(UIImage(named: "icHide"), for: .normal)
                loginCell.visiblePasswordButton.isSelected = false
                loginCell.visiblePasswordButton.addTarget(self, action: #selector(showHidePassword(_ :)), for: .touchUpInside)
            } else {
                loginCell.visiblePasswordButton.isHidden = true
                loginCell.inputTextField.isSecureTextEntry = false
            }
            
            if userModel.errorIndex == indexPath.row{
                loginCell.errorLabel.text = userModel.errorMessage
            } else{
                loginCell.errorLabel.text = ""
            }
            
            switch indexPath.row {
            case 0:
                loginCell.visiblePasswordButton.isHidden = true
                loginCell.inputTextField.text = userModel.email
                loginCell.inputTextField.keyboardType = .emailAddress
                loginCell.inputTextField.returnKeyType = .next
                loginCell.inputImageView.image = #imageLiteral(resourceName: "Message From Team")
                break
            case 1:
                loginCell.visiblePasswordButton.isHidden = false
                loginCell.inputTextField.text = userModel.password
                loginCell.inputTextField.keyboardType = .asciiCapable
                loginCell.inputTextField.returnKeyType = .done
                loginCell.inputTextField.isSecureTextEntry = true
                loginCell.inputImageView.image = #imageLiteral(resourceName: "key")
                break
            default:
                break
            }
            return loginCell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
        
        //UITextFieldDelegate methods
        func textFieldDidEndEditing(_ textField: UITextField) {
            switch textField.tag {
            case 100:
                userModel.email = textField.text!
                break
            case 101:
                userModel.password = textField.text!
                break
            default:
                break
            }
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let textLength = textField.text! + string
            
            
            switch textField.tag {
            case 100:
                if textLength.count > 150 {
                    return false
                } else {
                    return true
                }
            case 101:
                if textLength.count > 16 {
                    return false
                } else {
                    return true
                }
            default:
                break
            }
            return true
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField.returnKeyType == .next {
                let field = self.view.viewWithTag(textField.tag+1)
                field?.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
            return true
        }
        
    }



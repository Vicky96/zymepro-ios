//
//  AddDeviceSecondStepVC.swift
//  Zyme
//
//  Created by Vicky Singh on 17/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class AddDeviceSecondStepVC: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var secondStepTableView: UITableView!
    @IBOutlet weak var barCodeImageView: UIImageView!
    
    var userModel = UserDetailsModel()

        }

        // MARK: - View Life Cycle
        extension AddDeviceSecondStepVC {
            
            override func viewDidLoad() {
                super.viewDidLoad()
                // Do any additional setup after loading the view, typically from a nib.
                barCodeImageView.image = UIImage.gif(name: "bar-1")
                
            }
            
            override func viewWillAppear(_ animated: Bool) {
                
            }
            
            override func didReceiveMemoryWarning() {
                super.didReceiveMemoryWarning()
                // Dispose of any resources that can be recreated.
            }
            
            override var preferredStatusBarStyle: UIStatusBarStyle {
                return .lightContent
            }
            
        }
        // MARK: - Target Actions
        extension AddDeviceSecondStepVC {
            @objc func nextButton(_ sender: UIButton) {
                self.view.endEditing(true)
                print("Right Arrow Button tapped")
                
            }
            
        }

        // MARK: - IBActions
        extension AddDeviceSecondStepVC {
            @IBAction func scanAction(_ sender: UIButton ){
                
            }
            
            @IBAction func backAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
            }

        }


        // MARK: - Private Functions
        extension AddDeviceSecondStepVC {
           
        }

        // MARK: - Delegates
        extension AddDeviceSecondStepVC: UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
            
            //UITableViewDelegate,UITableViewDataSource methods
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return 1
            }
            
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let loginCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
                loginCell.errorLabel.text = ""
                loginCell.inputImageView.isHidden = true
                loginCell.visiblePasswordButton.isHidden = false
                loginCell.inputTextField.tweePlaceholder = "eg.21xxxxxxxx33"
                loginCell.visiblePasswordButton.addTarget(self, action: #selector(nextButton(_ :)), for: .touchUpInside)
                loginCell.inputTextField.keyboardType = .numberPad
                loginCell.inputTextField.returnKeyType = .done
                
                
       
                return loginCell
            }
            
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return UITableView.automaticDimension
            }
            
            //UITextFieldDelegate methods
            func textFieldDidEndEditing(_ textField: UITextField) {
                let str = textField.text!
                let trimmedString = str.trimmingCharacters(in: .whitespaces)
                userModel.barcode = trimmedString
                
            }
            
            func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                let textLength = textField.text! + string
                    if textLength.count > 16 {
                        return false
                    } else {
                        return true
                    }
            }
            
        }







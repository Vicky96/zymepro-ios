//
//  AddDeviceThirdStepVC.swift
//  Zyme
//
//  Created by Vicky Singh on 17/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class AddDeviceThirdStepVC: UIViewController {

        // MARK: - IBOutlet
        @IBOutlet weak var thirdStepTableView: UITableView!
        @IBOutlet weak var deviceImageView: UIImageView!
        
        
        // MARK: - View Life Cycle
        override func viewDidLoad() {
                super.viewDidLoad()
                // Do any additional setup after loading the view.
            
                setupUI()
                setupNotifications()
            }
            
            override func viewWillAppear(_ animated: Bool) {
                super.viewWillAppear(true)
            }
            override func viewWillDisappear(_ animated: Bool) {
                super.viewWillDisappear(true)
            }
            override func didReceiveMemoryWarning() {
                super.didReceiveMemoryWarning()
                // Dispose of any resources that can be recreated.
            }
            
            override var preferredStatusBarStyle: UIStatusBarStyle {
                return .lightContent
            }
        }

        // MARK: - Private Functions
        extension AddDeviceThirdStepVC {
            fileprivate func setupUI() {
                let labelCell = UINib.init(nibName: "singleLabelCell", bundle: nil)
                self.thirdStepTableView.register(labelCell, forCellReuseIdentifier: "singleLabelCell")
                deviceImageView.image = UIImage.gif(name: "device_plug-1")
                thirdStepTableView.reloadData()
                
            }
            
            fileprivate func setupNotifications() {
                
            }
            
        }
        // MARK: -  Target Actions
        extension AddDeviceThirdStepVC {
        }

        // Mark :- IBActions
        extension AddDeviceThirdStepVC {
            
            @IBAction func backButtonAction(_ sender: UIButton ){
    //            self.navigationController?.popWithFadeAnimation()
            }
            @IBAction func nextButtonAction(_ sender: UIButton ){
                
            }
        }



        extension AddDeviceThirdStepVC : UITableViewDelegate, UITableViewDataSource {
            
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return 3
            }
            
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let titlecell = tableView.dequeueReusableCell(withIdentifier: "singleLabelCell") as! singleLabelCell
                titlecell.titleLabel.textColor = .white
                if indexPath.row == 0 {
                    
                    titlecell.titleLabel.text = "Insert the device into the OBD port of your car"
                }else if indexPath.row == 1 {
                    titlecell.titleLabel.textColor = .blue
                    titlecell.titleLabel.text = "Can't find the OBD port?"
                    
                }else if indexPath.row == 2 {
                    titlecell.titleLabel.text = "After plugging the device firmly into the OBD port,please switch on the ignition of your car and click continue"
                }

                return titlecell
            }
            
            
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return UITableView.automaticDimension
            }
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
            }
        }

        // MARK: -  Functions
    extension AddDeviceThirdStepVC {
        
    }





//
//  RegisterVC.swift
//  Zyme
//
//  Created by Vicky Singh on 16/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {
        
        //IBOutlet
        @IBOutlet weak var viewControllerView: UIView!
        @IBOutlet weak var signupButton: UIButton!
        @IBOutlet weak var loginButton: UIButton!
        //Private Properties
        //Public Properties
        //Private Computed Properties
        var signupVC: SignupVC?
        var loginVC : LoginVC?
        var width = CGFloat()
    
        
    }

    // MARK: - View Life Cycle
    extension RegisterVC {
        
        override func viewDidLoad() {
            super.viewDidLoad()
            initialSetup()
            
            
            
            
            // Do any additional setup after loading the view, typically from a nib.
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
    }

    // MARK: - IBActions
    extension RegisterVC {
        @IBAction func signupButtonTapped(_ sender: Any) {
            InitialButtonSelected()
        }
        //    @IBAction func backButtonAction(_ sender: UIButton ){
        //        self.navigationController?.popWithFadeAnimation()
        //    }
        @IBAction func loginButtonTapped(_ sender: Any) {
            signupButton.backgroundColor = .black
            loginButton.backgroundColor = #colorLiteral(red: 0.08538616449, green: 0.4800409675, blue: 0.9989228845, alpha: 1)
            
            
            for component in viewControllerView.subviews{
                
                component.removeFromSuperview()
                
            }
            
            loginVC?.view.frame = self.viewControllerView.bounds
            
            self.viewControllerView.addSubview((loginVC?.view)!)
            
            self.addChild(loginVC!)
            
        }
    }

    // MARK: - Target Actions
    extension RegisterVC {
        
    }

    // MARK: - Functions
    extension RegisterVC {
        @objc func loadList(){
            //load data here
            self.viewWillAppear(true)
        }
        func initialSetup(){
            
            viewControllerSetup()
            
            for component in viewControllerView.subviews{
                
                component.removeFromSuperview()
                
            }
            
            signupVC?.view.frame = self.viewControllerView.bounds
            self.viewControllerView.addSubview((signupVC?.view)!)
            self.addChild(signupVC!)
            let bounds = UIScreen.main.bounds
            width = bounds.size.width
            InitialButtonSelected()
        }
        func viewControllerSetup(){
            signupVC = storyboard?.instantiateViewController(withIdentifier: "SignupVC") as? SignupVC
            loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
            
        }
        //    func handleTap(sender: UIButton) {
        //
        //        if sender.backgroundColor == UIColor.RGBA(r: 229, g: 35, b: 129) {
        //            sender.backgroundColor = UIColor.RGBA(r: 170, g: 170, b: 170)
        //        } else {
        //            sender.backgroundColor = UIColor.RGBA(r: 229, g: 35, b: 129)
        //        }
        //    }
        
    }

    // MARK: - Function
    extension RegisterVC {
        func InitialButtonSelected() {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "activitySelected"), object: nil)
            loginButton.backgroundColor = .black
            signupButton.backgroundColor = #colorLiteral(red: 0.08538616449, green: 0.4800409675, blue: 0.9989228845, alpha: 1)
            for component in viewControllerView.subviews{
                
                component.removeFromSuperview()
                
            }
            
            signupVC?.view.frame = self.viewControllerView.bounds
            
            self.viewControllerView.addSubview((signupVC?.view)!)
            
            self.addChild(signupVC!)
        }
        
}

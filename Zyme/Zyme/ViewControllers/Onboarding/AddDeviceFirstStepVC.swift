//
//  AddDeviceFirstStepVC.swift
//  Zyme
//
//  Created by Vicky Singh on 17/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class AddDeviceFirstStepVC: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var firstStepTableView: UITableView!
    
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
            
            setupUI()
            setupNotifications()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
        }
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(true)
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
    }

    // MARK: - Private Functions
    extension AddDeviceFirstStepVC {
        fileprivate func setupUI() {
            let labelCell = UINib.init(nibName: "singleLabelCell", bundle: nil)
            self.firstStepTableView.register(labelCell, forCellReuseIdentifier: "singleLabelCell")
            let firstStepCell = UINib.init(nibName: "AddDeviceFirstStepCell", bundle: nil)
            self.firstStepTableView.register(firstStepCell, forCellReuseIdentifier: "AddDeviceFirstStepCell")
            firstStepTableView.reloadData()
            
        }
        
        fileprivate func setupNotifications() {
            
        }
        
    }
    // MARK: -  Target Actions
    extension AddDeviceFirstStepVC {
    }

    // Mark :- IBActions
    extension AddDeviceFirstStepVC {
        
        @IBAction func backButtonAction(_ sender: UIButton ){
//            self.navigationController?.popWithFadeAnimation()
        }
        @IBAction func nextButtonAction(_ sender: UIButton ){
            
        }
    }



    extension AddDeviceFirstStepVC : UITableViewDelegate, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 6
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let titlecell = tableView.dequeueReusableCell(withIdentifier: "singleLabelCell") as! singleLabelCell
            let firstStepCell = tableView.dequeueReusableCell(withIdentifier: "AddDeviceFirstStepCell") as! AddDeviceFirstStepCell
                firstStepCell.downArrowButton.isHidden = false
            if indexPath.row == 0 {
                titlecell.titleLabel.textColor = .white
                titlecell.titleLabel.text = "The one-time activation of your device will take less than a minute"
                return titlecell
            } else if indexPath.row == 1 {
                firstStepCell.titleLabel.text = "Scan device code"
                
            } else if indexPath.row == 2 {
                firstStepCell.titleLabel.text = "Plug device in your car"
                
            } else if indexPath.row == 3 {
                firstStepCell.titleLabel.text = "Start your car"
                
            } else if indexPath.row == 4 {
                firstStepCell.titleLabel.text = "Register your car"
                
            } else if indexPath.row == 5 {
                firstStepCell.titleLabel.text = "Activate device"
                firstStepCell.downArrowButton.isHidden = true
            }

            return firstStepCell
        }
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        }
    }

    // MARK: -  Functions
extension AddDeviceFirstStepVC {
    
}



//
//  CustomButtonCell.swift
//  Zyme
//
//  Created by Vicky Singh on 18/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class CustomButtonCell: UITableViewCell {
    
    
    @IBOutlet weak var topViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var topViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var seperatorViewFirst: UIView!
    @IBOutlet weak var seperatorViewSecond: UIView!
    @IBOutlet weak var buttonFirst: UIButton!
    @IBOutlet weak var buttonSecond: UIButton!
    @IBOutlet weak var buttonThird: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  TextFieldCell.swift
//  Zyme
//
//  Created by Vicky Singh on 16/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {

        @IBOutlet weak var inputTextField: TweeAttributedTextField!
        @IBOutlet weak var errorLabel: UILabel!
        @IBOutlet weak var visiblePasswordButton: UIButton!
        @IBOutlet weak var inputImageView: UIImageView!
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
        
    }

//
//  ViewControllerAccessors.swift
//  Zyme
//
//  Created by Vicky Singh on 17/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit

class ViewControllerAccessors: NSObject {
    
    static var loginViewController: LoginVC {
        if let viewController = StoryboardAccessors.onboardingStoryboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
            return viewController
        } else {
            return LoginVC()
        }
    }
    
    static var SignUpViewController: SignupVC {
        if let viewController = StoryboardAccessors.onboardingStoryboard.instantiateViewController(withIdentifier: "SignupVC") as? SignupVC {
            return viewController
        } else {
            return SignupVC()
        }
    }
    
    static var addDeviceFirstStepVC: AddDeviceFirstStepVC {
        if let viewController = StoryboardAccessors.onboardingStoryboard.instantiateViewController(withIdentifier: "AddDeviceFirstStepVC") as? AddDeviceFirstStepVC {
            return viewController
        } else {
            return AddDeviceFirstStepVC()
        }
    }
    
    
    
//    static var forgotPasswordViewController: ForgotPasswordVC {
//        if let viewController = StoryboardAccessors.baseStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC {
//            return viewController
//        } else {
//            return ForgotPasswordVC()
//        }
//    }
//    static var tabBarController: TabBarController {
//        if let viewController = StoryboardAccessors.tabbarStoryboard.instantiateViewController(withIdentifier: "TabBarController") as? TabBarController {
//            return viewController
//        } else {
//            return TabBarController()
//        }
//    }
    
//    static var settingViewController: SettingsVC {
//        if let viewController = StoryboardAccessors.tabbarStoryboard.instantiateViewController(withIdentifier: "SettingsVC") as? SettingsVC {
//            return viewController
//        } else {
//            return SettingsVC()
//        }
//    }
    
  
    
//    static var profileViewController: ProfileVC {
//        if let viewController = StoryboardAccessors.settingsStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC {
//            return viewController
//        } else {
//            return ProfileVC()
//        }
//    }
   
}

extension UIViewController {
    func presentOnRoot(`with` viewController : UIViewController){
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
}

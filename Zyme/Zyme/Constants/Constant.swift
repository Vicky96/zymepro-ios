//
//  Constant.swift
//  Zyme
//
//  Created by Vicky Singh on 16/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import UIKit
import CommonCrypto

class Constant: NSObject {
    struct UserDefault {
        static let appOpenFirstTime = "appOpenFirstTime"
    }
}

//MARK:- Global Keys
let appdelegate = UIApplication.shared.delegate as! AppDelegate
let defaults = UserDefaults.standard
let notificationCenter = NotificationCenter.default
let Window_Width = UIScreen.main.bounds.size.width
let Window_Height = UIScreen.main.bounds.size.height
var generalVCBluetoothState = Bool()
var alertVCBluetoothState = Bool()
var fabBluetoothStateConfig = Bool()


//MARK: - Storyboard Constants
let base = UIStoryboard(name: "Base", bundle: nil)

//MARK: -  Fonts Constant
let navTitleFont = lightFont(17)

func lightFont(_ size: Float) -> UIFont {
    return UIFont(name: "TwCenMTStd-Light", size: CGFloat(size))!
}

func semiboldFont(_ size: Float) -> UIFont {
    return UIFont(name: "TwCenMTStd-MediumCond", size: CGFloat(size))!
}

func boldFont(_ size: Float) -> UIFont {
    return UIFont(name: "TwCenMTStd-Bold", size: CGFloat(size))!
}

func italicFont(_ size: Float) -> UIFont {
    return UIFont(name: "TwCenMTStd-Italic", size: CGFloat(size))!
}

func mediumFont(_ size: Float) -> UIFont {
    return UIFont(name: "TwCenMTStd-SemiMedium", size: CGFloat(size))!
}

func regularFont(_ size: Float) -> UIFont {
    return UIFont(name: "TwCenMTStd", size: CGFloat(size))!
}

//MARK:- Color Constants
//let lightGreenColor = UIColor.RGBA(r: 0, g: 181, b: 0)
//let darkBlueColor   = UIColor.RGBA(r: 0, g: 0, b: 141)
//let lightBgColor    = UIColor.RGBA(r: 245, g: 245, b: 245)
//let searchBarBgColor = UIColor.RGBA(r: 255, g: 107, b: 116)


let requestTimeOut = "Request Timeout!"
let notInternetText = "Please check your internet connection and try again."


//MARK:- Controllers
let loginVC = "LoginVc"


//MARK:- Validation Messages
let enterName = "*Please enter Name."
let validName = "*Please enter valid Name."
let enterEmail = "*Please enter Email."
let validEmail = "*Please enter valid Email."
let enterNumber = "*Please enter phone number."
let validNumber = "*Please enter valid phone number."
let enterPassword  = "*Please enter password."
let validPassword  = "*Please enter valid password."
let enterOldPassword  = "*Please enter old password."
let validOldPassword  = "*Please enter valid old password."
let enterCurrentPassword = "*Please enter current password."
let passwordMinLength = "*Password must contain minimum 6 characters."
let enterNewPassword = "*Please enter new password."
let currentNewErrorMessage = "*Password does not match."
let enterConfirmPassword = "*Please enter confirm password."
let newConfirmErrorMessage = "*Both the passwords must be same."
let confirmPassword = "*Both the passwords must be same."
let changePasswordText = "*Do you want to change password?"
let chooseNewPassword = "*Please choose new password"
let enterOTP = "*Please enter OTP."
let validOTP = "*Please enter valid OTP."



extension Data {
    
    
    func hexString() -> String {
        let string = self.map{String(format:"%02x", Int($0))}.joined()
        return string
    }
    
    
    func SHA1() -> Data {
        var result = Data(count: Int(CC_SHA1_DIGEST_LENGTH))
        _ = result.withUnsafeMutableBytes {resultPtr in
            self.withUnsafeBytes {(bytes: UnsafePointer<UInt16>) in
                CC_SHA1(bytes, CC_LONG(count), resultPtr)
            }
        }
        return result
    }
    
}

extension String {
    
    func hexString() -> String {
        return self.data(using: .utf8)!.hexString()
    }
    
    func SHA1() -> String {
        return self.data(using: .utf8)!.SHA1().hexString()
    }
    
}

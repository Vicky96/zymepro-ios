//
//  StringExtensions.swift
//  Zyme
//
//  Created by Vicky Singh on 16/01/20.
//  Copyright © 2020 Vicky Singh. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func contains(_ string: String) -> Bool {
        return range(of: string) != nil
    }
    
    func substringFromIndex(_ index: Int) -> String {
        if index < 0 || index > count {
            //print("index \(index) out of bounds")
            return ""
        }
        return "\(self[self.index(self.startIndex, offsetBy: index)])"
//        return self.substring(from: self.index(self.startIndex, offsetBy: index))
    }
    
    func substringToIndex(_ index: Int) -> String {
        if index < 0 || index > count {
            //print("index \(index) out of bounds")
            return ""
        }
        return "\(self[..<self.index(self.startIndex, offsetBy: index)])"
//        return self.substring(to: self.index(self.startIndex, offsetBy: index))
    }
    
    func trimWhiteSpace() -> String {
        let trimmedString = trimmingCharacters(in: CharacterSet.whitespaces)
        return trimmedString
    }
    
    func isValidMobileNumber() -> Bool {
        let mobileNoRegEx = "^((\\+)|(00)|(\\*)|())[0-9٠-٩]{8,14}((\\#)|())$"
        let mobileNoTest = NSPredicate(format: "SELF MATCHES %@", mobileNoRegEx)
        return mobileNoTest.evaluate(with: self)
    }
    
    func isContainsAllZeros() -> Bool {
        let mobileNoRegEx = "^0*$"
        let mobileNoTest = NSPredicate(format: "SELF MATCHES %@", mobileNoRegEx)
        return mobileNoTest.evaluate(with: self)
    }
    
    func isValidUserName() -> Bool {
        let nameRegEx = "^[a-zA-Z0-9\\s]+$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        let passwordRegEx = "^[A-Za-z0-9]+(?=.*?)(?=.*?[#?!@$%^&*-]),{8,16}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    
    func isEmail() -> Bool {
        let emailRegex = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        print(emailTest.evaluate(with: self))
        return emailTest.evaluate(with: self)
    }
    
    func isValidLastName() -> Bool {
        let passwordRegEx = "[a-zA-z]+([ '-][a-zA-Z]+)*"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    
    func isContainsAtleastOneSpecialCharacters() -> Bool {
        let passwordRegEx = "^(?=.*?[$@$!%*#?&^)(]).{8,}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    
    func containsAlphaNumericOnly() -> Bool {
        let nameRegEx = "^[a-zA-Z0-9\\s]+$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: self)
    }
    
    func containsNumberOnly() -> Bool {
        let nameRegEx = "^[0-9]+$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: self)
    }
    
    func containsAlphabetsOnly() -> Bool {
        let nameRegEx = "^[a-zA-Z]+$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: self)
    }
    
    func isValidName() -> Bool {
        let nameRegEx = "^[a-zA-Zء-ي\\s]+$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: self)
    }
    
    var length: Int {
        return count
    }
    
    func dateFromString(_ format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        if let date = dateFormatter.date(from: self) {
            return date
        } else {
            Debug.log("Unable to format date")
        }
        
        return nil
    }
    
    func changeDateFormatToMonth(format: String) -> String {
        //from format
        //required
        let getFormat = DateFormatter()
        getFormat.dateFormat = "dd-MM-yyyy"
        let getDate = getFormat.date(from: self)
        
        if getDate == nil {
            return ""
        }
        
        //required
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        let requiredString = formatter.string(from: getDate!)
        
        return requiredString
    }
    
//    func md5() -> String {
//        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
//        if let data = self.dataUsingEncoding(NSUTF8StringEncoding) {
//            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
//        }
//
//        var digestHex = ""
//        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
//            digestHex += String(format: "%02x", digest[index])
//        }
//
//        return digestHex
//    }
    
    //>>>> removes all whitespace from a string, not just trailing whitespace <<<//
    func removeWhitespace() -> String {
        return replaceString(" ", withString: "")
    }
    
    //>>>> Replacing String with String <<<//
    func replaceString(_ string: String, withString: String) -> String {
        return replacingOccurrences(of: string, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func getAttributedString(_ string_to_Attribute: String, color: UIColor, font: UIFont) -> NSAttributedString {
        let range = (self as NSString).range(of: string_to_Attribute)
        
        let attributedString = NSMutableAttributedString(string: self)
        
        // multiple attributes declared at once
        let multipleAttributes = [
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font: font
        ]
        
        attributedString.addAttributes(multipleAttributes, range: range)
        
        return attributedString.mutableCopy() as! NSAttributedString
    }
    
    //for arabic content
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.width)
    }
    
    
    func setAttributedText(_ lineSpacing: CGFloat) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        return attributedString
    }
    
}

extension String {
    func separate(every stride: Int , with separator: Character) -> String {
        return String(enumerated().map { $0 > 0 && $0 % stride == 0 ? [separator, $1] : [$1]}.joined())
    }
}

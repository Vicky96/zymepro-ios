//
//  UserDetailsModel.swift
//  PlayFit
//
//  Created by Vicky Singh on 16/09/19.
//  Copyright © 2019 Vicky Singh. All rights reserved.
//

import UIKit

class UserDetailsModel: NSObject {

    var email = ""
    var isAgreeSelected = true
    var mobile = ""
    var macAddr = ""
    var name = ""
    var barcode = ""
    var profilePic = UIImage(named: "")
    var imageNameString = ""
    var uploadImageUrl = ""
    var uploadImageData = Data()
    var password  = ""
    var petName = ""
    var vehicleNumber = ""
    var confirmPassword = ""
    var oTP = ""
    var oldPassword = ""
    var isFromSetting = false
    var maleIsSelected = true
    var femaleIsSelected = false
    var dateOfBirth = ""
    var gender = ""
    var petrolIsSelected = Bool()
    var dieselIsSelected = Bool()
   
    
    var errorIndex = -1
    var errorMessage = ""
}
